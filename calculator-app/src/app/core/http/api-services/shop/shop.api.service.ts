import {Injectable} from '@angular/core';
import {HttpService} from '../../http.service';
import {Observable} from 'rxjs';
import {CalculatorApiResult} from '../../../../modules/calculator/models/calculator-api-result.model';

@Injectable({
    providedIn: 'root'
})
export class ShopApiService {
    private _BASE_URL = 'shop';

    constructor(private _httpService: HttpService) {
    }

    searchCombination$(shopId: number, queryParams?: any): Observable<CalculatorApiResult> {
        return this._httpService
            .get(`${this._BASE_URL}/${shopId}/search-combination`, {params: queryParams});
    }
}
