import {TestBed, waitForAsync} from '@angular/core/testing';
import {ShopApiService} from './shop.api.service';
import {HttpService} from '../../http.service';
import {Observable} from 'rxjs';

class HttpServiceMock {
    get() {}
}

describe('ShopApiService', () => {
    let service: ShopApiService;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            providers: [
                ShopApiService,
                {
                    provide: HttpService,
                    useClass: HttpServiceMock
                }
            ]
        });
        service = TestBed.inject(ShopApiService);
    }));

    describe('searchCombination$()', () => {
        it('should call this._httpService().get()', () => {
            spyOn(service['_httpService'], 'get')
                .and.returnValue(new Observable((observer => {
                observer.next({});
                observer.complete();
            })));
            const shopId = 10;
            const queryParams = {amount: 10};
            service
                .searchCombination$(shopId, queryParams)
                .subscribe(() => {
                    const expectedUrl = `shop/${shopId}/search-combination`;
                    const expectedOptions = {params: queryParams}
                    expect(service['_httpService'].get).toHaveBeenCalledWith(expectedUrl, expectedOptions)
                });
        });
    });
});
