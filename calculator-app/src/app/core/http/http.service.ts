import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class HttpService {
    private _BASE_URL = `${environment.apiUrl}`;

    constructor(private _http: HttpClient) { }

    /**
     * Generic method which does a http request according to method and url parms
     * @param method GET, POST, PATCH, DELETE
     * @param url route url
     * @param body for POST, PATCH
     * @param options for query params etc...
     * @private
     */
    private _request(method: string, url: string, body?: any, options?: any): Observable<any> {
        return new Observable((observer: any) => {
            options = options || {};
            if (body) {
                options = {...options, body};
            }
            this._http.request(method, url, options)
                .subscribe(response => {
                        observer.next(response);
                        observer.complete();
                    },
                    error => {
                        observer.error(error);
                    }
                );
        });
    }

    /**
     * Make a GET http request
     * @param url route url
     * @param options for query params etc...
     */
    get(url: string, options?: any): Observable<any> {
        return this._request('GET', `${this._BASE_URL}/${url}`, null, options);
    }
}
