import {TestBed, waitForAsync} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpService} from './http.service';

describe('HttpService', () => {
    let service: HttpService;
    let httpTestingController: HttpTestingController;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                HttpService
            ]
        })
            .compileComponents();

        service = TestBed.inject(HttpService);
        httpTestingController = TestBed.inject(HttpTestingController);
    }));

    afterEach(() => {
        httpTestingController.verify();
    });

    describe('_request()', () => {
        it('should make an http request according to params', () => {
            const method = 'POST';
            const url = 'test';
            const body = {test: 'test'};
            service
                ['_request'](method, url, body)
                .subscribe((res: any) => {
                    expect(res).toEqual({data: {}});
                });

            const req = httpTestingController.expectOne(url);
            expect(req.request.method).toEqual(method);
            expect(req.request.body).toEqual(body);

            const reqResponse = {
                data: {}
            };
            req.flush(reqResponse);
        });
    });

    describe('get()', () => {
        it('should call this._request()', () => {
            // @ts-ignore
            spyOn(service, '_request');
            service['_BASE_URL'] = 'http://localhost:3000';
            const url = 'test';
            service.get(url, {params: {}});
            const expectedUrl = 'http://localhost:3000/test';
            expect(service['_request']).toHaveBeenCalledWith('GET', expectedUrl, null, {params: {}});
        });
    });
});
