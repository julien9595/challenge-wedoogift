import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
    constructor(private _authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Get the api token from AuthenticationService
        const token: string = this._authenticationService.apiToken;
        request = request.clone({
            setHeaders: {
                Authorization: `${token}`
            }
        });
        return next.handle(request);
    }
}
