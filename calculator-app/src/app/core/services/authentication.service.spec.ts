import {TestBed, waitForAsync} from '@angular/core/testing';
import {AuthenticationService} from './authentication.service';

describe('AuthenticationService', () => {
    let service: AuthenticationService;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            providers: [
                AuthenticationService
            ]
        });
        service = TestBed.inject(AuthenticationService);
    }));

    it('should create', () => {
        expect(service).toBeTruthy();
    });

    describe('apiToken()', () => {
        it('should return this._TOKEN value', async () => {
            service['_TOKEN'] = '123';
            expect(service.apiToken).toEqual('123');
        });
    });
});
