import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private _TOKEN: string = 'tokenTest123';

    constructor() {
    }

    get apiToken(): string {
        return this._TOKEN;
    }
}
