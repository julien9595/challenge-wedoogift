import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HomeComponent} from './home.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {By} from '@angular/platform-browser';

describe('HomeComponent', () => {
    let component: HomeComponent;
    let fixture: ComponentFixture<HomeComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                MatToolbarModule
            ],
            declarations: [ HomeComponent ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`should have as title 'calculator-app'`, () => {
        expect(component.title).toEqual('calculator-app');
    });

    it('should render title inside mat-toolbar', () => {
        const toolbarElement = fixture.debugElement.query(By.css('#toolbar'))?.nativeElement as HTMLElement;
        expect(toolbarElement).toBeTruthy();
        expect(toolbarElement.querySelector('span')?.textContent).toContain('calculator-app');
    });
});
