import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {CalculatorModule} from '../../calculator/components/calculator.module';

@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        CommonModule,
        MatToolbarModule,
        CalculatorModule
    ],
    exports: [
        HomeComponent
    ],
})
export class HomeModule {}
