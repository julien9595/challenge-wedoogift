import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CalculatorComponent} from './calculator.component';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatListModule} from '@angular/material/list';
import {CalculatorService} from '../services/calculator.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {By} from '@angular/platform-browser';
import {CalculatorComponentValue} from '../models/calculator-component-value.model';
import {CalculatorApiResult} from '../models/calculator-api-result.model';

class CalculatorServiceMock {
    searchCombination() {}
}

describe('CalculatorComponent', () => {
    let component: CalculatorComponent;
    let fixture: ComponentFixture<CalculatorComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                BrowserAnimationsModule,
                CommonModule,
                ReactiveFormsModule,
                MatFormFieldModule,
                MatInputModule,
                MatButtonModule,
                MatCardModule,
                MatProgressSpinnerModule,
                MatListModule
            ],
            declarations: [ CalculatorComponent ],
            providers: [
                {
                    provide: CalculatorService,
                    useClass: CalculatorServiceMock
                },
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CalculatorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('div#correctAmountResultBlock', () => {
        it('should not render div#correctAmountResultBlock if this.calculatorResult is null', () => {
            // @ts-ignore
            component.calculatorResult = null;
            fixture.detectChanges();
            const divElement = fixture.debugElement.query(By.css('#correctAmountResultBlock'));
            expect(divElement).toBeNull();
        });

        it('should not render div#correctAmountResultBlock if this.calculatorResult is undefined', () => {
            // @ts-ignore
            component.calculatorResult = undefined;
            fixture.detectChanges();
            const divElement = fixture.debugElement.query(By.css('div#correctAmountResultBlock'));
            expect(divElement).toBeNull();
        });

        it('should not render div#correctAmountResultBlock if this.calculatorResult.equal is null', () => {
            // @ts-ignore
            component.calculatorResult = {equal: null};
            fixture.detectChanges();
            const divElement = fixture.debugElement.query(By.css('div#correctAmountResultBlock'));
            expect(divElement).toBeNull();
        });

        it('should not render div#correctAmountResultBlock if this.calculatorResult.equal is undefined', () => {
            component.calculatorResult = {};
            fixture.detectChanges();
            const divElement = fixture.debugElement.query(By.css('div#correctAmountResultBlock'));
            expect(divElement).toBeNull();
        });

        it('should render div#correctAmountResultBlock if this.calculatorResult.equal exists', () => {
            component.calculatorResult = {equal: {value: 0, cards: []}};
            fixture.detectChanges();
            const divElement = fixture.debugElement.query(By.css('#correctAmountResultBlock'));
            expect(divElement).toBeTruthy();
        });
    });

    describe('div#otherPossibleAmounts', () => {
        it('should not render div#otherPossibleAmounts if this.calculatorResult is null', () => {
            // @ts-ignore
            component.calculatorResult = null;
            fixture.detectChanges();
            const divElement = fixture.debugElement.query(By.css('#otherPossibleAmounts'));
            expect(divElement).toBeNull();
        });

        it('should not render div#otherPossibleAmounts if this.calculatorResult is undefined', () => {
            // @ts-ignore
            component.calculatorResult = undefined;
            fixture.detectChanges();
            const divElement = fixture.debugElement.query(By.css('div#otherPossibleAmounts'));
            expect(divElement).toBeNull();
        });

        it('should render div#otherPossibleAmounts if this.calculatorResult.equal is null', () => {
            // @ts-ignore
            component.calculatorResult = {equal: null};
            fixture.detectChanges();
            const divElement = fixture.debugElement.query(By.css('div#otherPossibleAmounts'));
            expect(divElement).toBeTruthy();
        });

        it('should render div#otherPossibleAmounts if this.calculatorResult.equal is undefined', () => {
            component.calculatorResult = {};
            fixture.detectChanges();
            const divElement = fixture.debugElement.query(By.css('div#otherPossibleAmounts'));
            expect(divElement).toBeTruthy();
        });
    });

    describe('button#plusButton', () => {
        it('should be disabled if this.isLoading equals true', () => {
            component.isLoading = true;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#plusButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeTrue();
        });

        it('should not be disabled if this.isLoading equals false', () => {
            component.isLoading = false;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#plusButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeFalse();
        });
    });

    describe('button#minusButton', () => {
        it('should be disabled if this.isLoading equals true', () => {
            component.isLoading = true;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#minusButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeTrue();
        });

        it('should be disabled if this.amount is null', () => {
            // @ts-ignore
            component.amount = null;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#minusButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeTrue();
        });

        it('should be disabled if this.amount is undefined', () => {
            // @ts-ignore
            component.amount = undefined;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#minusButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeTrue();
        });

        it('should be disabled if this.amount equals 0', () => {
            component.amount = 0;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#minusButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeTrue();
        });

        it('should not be disabled if this.isLoading equals false', () => {
            component.amount = 1;
            component.isLoading = false;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#minusButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeFalse();
        });

        it('should not be disabled if this.amount > 0', () => {
            component.isLoading = false;
            component.amount = 23;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#minusButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeFalse();
        });
    });

    describe('button#validateButton', () => {
        it('should be disabled if this.isLoading equals true', () => {
            component.isLoading = true;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#validateButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeTrue();
        });

        it('should be disabled if this.amount is null', () => {
            // @ts-ignore
            component.amount = null;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#validateButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeTrue();
        });

        it('should be disabled if this.amount is undefined', () => {
            // @ts-ignore
            component.amount = undefined;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#validateButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeTrue();
        });

        it('should be disabled if this.amount equals 0', () => {
            component.amount = 0;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#validateButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeTrue();
        });

        it('should not be disabled if this.isLoading equals false', () => {
            component.amount = 1;
            component.isLoading = false;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#validateButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeFalse();
        });

        it('should not be disabled if this.amount > 0', () => {
            component.isLoading = false;
            component.amount = 23;
            fixture.detectChanges();
            const disabledAttribute = fixture.debugElement.query(By.css('#validateButton')).nativeElement.disabled;
            expect(disabledAttribute).toBeFalse();
        });
    });

    describe('_updateAmount()', () => {
        beforeEach(() => { component.amount = 10 });

        it('should set this.amount if newAmount parameter is null', () => {
            // @ts-ignore
            component['_updateAmount'](null);
            expect(component.amount).toBeNull();
        });

        it('should set this.amount if newAmount parameter is undefined', () => {
            // @ts-ignore
            component['_updateAmount'](undefined);
            expect(component.amount).toBeNull();
        });

        it('should set this.amount if newAmount parameter is defined', () => {
            component['_updateAmount'](5);
            expect(component.amount).toEqual(5);
        });
    });

    describe('_handleCalculatorResultFromAPI()', () => {
        it('should not throw error if result param is null', () => {
            // @ts-ignore
            expect(() => component['_handleCalculatorResultFromAPI'](null)).not.toThrow();
        });

        it('should not throw error if result param is undefined', () => {
            // @ts-ignore
            expect(() => component['_handleCalculatorResultFromAPI'](undefined)).not.toThrow();
        });

        it('should call this.onSelectionChange() with result.floor if result.equal and result.ceil are undefined but not result.floor', () => {
            spyOn(component, 'onSelectionChange');
            const floor: CalculatorComponentValue = {value: 70, cards: [35, 35]};
            const result: CalculatorApiResult = {floor};
            component['_handleCalculatorResultFromAPI'](result);
            expect(component.onSelectionChange).toHaveBeenCalledWith(floor);
        });

        it('should call this.onSelectionChange() with result.floor if result.equal and result.floor are undefined but not result.ceil', () => {
            spyOn(component, 'onSelectionChange');
            const ceil: CalculatorComponentValue = {value: 20, cards: [20]};
            const result: CalculatorApiResult = {ceil};
            component['_handleCalculatorResultFromAPI'](result);
            expect(component.onSelectionChange).toHaveBeenCalledWith(ceil);
        });

        it('should update this.calculatorResult with result if result.ceil and result.floor are defined but not result.equal', () => {
            component.calculatorResult = {};
            const ceil: CalculatorComponentValue = {value: 25, cards: [25]};
            const floor: CalculatorComponentValue = {value: 22, cards: [22]};
            const result: CalculatorApiResult = {ceil, floor};
            component['_handleCalculatorResultFromAPI'](result);
            expect(component.calculatorResult).toEqual(result);
        });

        it('should update this.calculatorResult with result if result.equal is defined', () => {
            component.calculatorResult = {};
            const equal: CalculatorComponentValue = {value: 20, cards: [20]};
            const result: CalculatorApiResult = {equal};
            component['_handleCalculatorResultFromAPI'](result);
            expect(component.calculatorResult).toEqual(result);
        });
    });

    describe('manageAmount()', () => {
        beforeEach(() => {
            spyOn(component, 'validate');
        });

        describe('action parameter equals plus', () => {
            it('should set this.amount to 1 if this.amount is null', () => {
                // @ts-ignore
                component.amount = null;
                component.manageAmount('plus');
                expect(component.amount).toEqual(1);
            });

            it('should set this.amount to 1 if this.amount is undefined', () => {
                // @ts-ignore
                component.amount = undefined;
                component.manageAmount('plus');
                expect(component.amount).toEqual(1);
            });

            it('should set this.amount to 1 if this.amount equals 0', () => {
                component.amount = 0;
                component.manageAmount('plus');
                expect(component.amount).toEqual(1);
            });

            it('should increase this.amount by 1 if this.amount > 0', () => {
                component.amount = 24;
                component.manageAmount('plus');
                expect(component.amount).toEqual(25);
            });

            afterEach(() => {
                expect(component.validate).toHaveBeenCalled();
            });
        });

        describe('action parameter equals minus', () => {
            it('should not update this.amount if this.amount is null', () => {
                // @ts-ignore
                component.amount = null;
                component.manageAmount('minus');
                expect(component.amount).toBeNull();
                expect(component.validate).not.toHaveBeenCalled();
            });

            it('should not update this.amount if this.amount is undefined', () => {
                // @ts-ignore
                component.amount = undefined;
                component.manageAmount('minus');
                expect(component.amount).toBeUndefined();
                expect(component.validate).not.toHaveBeenCalled();
            });

            it('should not update this.amount if this.amount equals 0', () => {
                component.amount = 0;
                component.manageAmount('minus');
                expect(component.amount).toEqual(0);
                expect(component.validate).not.toHaveBeenCalled();
            });

            it('should increase this.amount by 1 if this.amount > 0', () => {
                component.amount = 12;
                component.manageAmount('minus');
                expect(component.amount).toEqual(11);
            });
        });
    });

    describe('onSelectionChange()', () => {
        it('should not throw error if newCalculatorValue param is null', () => {
            // @ts-ignore
            expect(() => component.onSelectionChange(null)).not.toThrow();
        });

        it('should not throw error if newCalculatorValue param is undefined', () => {
            // @ts-ignore
            expect(() => component.onSelectionChange(undefined)).not.toThrow();
        });

        it('should call this._updateAmount() with newCalculatorValue.value', () => {
            // @ts-ignore
            spyOn(component, '_updateAmount');
            const newCalculatorValue: CalculatorComponentValue = {value: 20, cards: [20]}
            component.onSelectionChange(newCalculatorValue);
            expect(component['_updateAmount']).toHaveBeenCalledWith(20);
        });

        it('should update this.calculatorResult', () => {
            // @ts-ignore
            spyOn(component, '_updateAmount');
            component.calculatorResult = {};
            const newCalculatorValue: CalculatorComponentValue = {value: 20, cards: [20]}
            component.onSelectionChange(newCalculatorValue);
            const expectedResult = {equal: {value: 20, cards: [20]}};
            expect(component.calculatorResult).toEqual(expectedResult);
        });
    });

    describe('validate()', () => {
        it('should call this._calculatorService.searchCombination()', async () => {
            spyOn(component['_calculatorService'], 'searchCombination')
                .and.returnValue(Promise.resolve({}));
            component.amount = 45;
            await component.validate();
            expect(component['_calculatorService'].searchCombination).toHaveBeenCalledWith(5, 45);
        });

        it('should call this._handleCalculatorResultFromAPI() with this._calculatorService.searchCombination() returned value', async () => {
            const returnedValue: CalculatorApiResult = {ceil: {value: 20, cards: [20]}};
            spyOn(component['_calculatorService'], 'searchCombination')
                .and.returnValue(Promise.resolve(returnedValue));
            // @ts-ignore
            spyOn(component, '_handleCalculatorResultFromAPI');
            component.amount = 45;
            await component.validate();
            expect(component['_handleCalculatorResultFromAPI']).toHaveBeenCalledWith(returnedValue);
        });
    });
});
