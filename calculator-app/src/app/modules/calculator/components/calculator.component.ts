import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CalculatorService} from '../services/calculator.service';
import {CalculatorApiResult} from '../models/calculator-api-result.model';
import {CalculatorComponentValue} from '../models/calculator-component-value.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-calculator',
    templateUrl: './calculator.component.html',
    styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
    form!: FormGroup;
    calculatorResult!: CalculatorApiResult;
    isLoading: boolean = false;

    get amount(): number {
        return this.form.controls.amount.value;
    }

    set amount(newAmount: number) {
        this.form.controls.amount.patchValue(newAmount);
    }

    @Output() amountChange: EventEmitter<number> = new EventEmitter<number>();

    constructor(private _formBuilder: FormBuilder,
                private _calculatorService: CalculatorService) {
    }

    ngOnInit(): void {
        this._initForm();
    }

    private _initForm(): void {
        this.form = this._formBuilder.group({
            amount: [null, Validators.required]
        });
    }

    /**
     * Update this.form.controls.amount to newAmount value
     * @param newAmount value
     * @private
     */
    private _updateAmount(newAmount: number): void {
        this.amount = newAmount ?? null;
    }

    /**
     * Decide what to do according to result from API
     * @param result
     * @private
     */
    private _handleCalculatorResultFromAPI(result: CalculatorApiResult): void {
        if (result) {
            if (!result.equal && result.floor && !result.ceil) {
                // If the desired amount is lower than the possible amounts
                this.onSelectionChange(result.floor);
            } else if (!result.equal && result.ceil && !result.floor) {
                // If the desired amount is higher than the possible amounts
                this.onSelectionChange(result.ceil);
            } else {
                // If the desired amount is possible or not
                this.calculatorResult = result;
                this.amountChange.emit(this.amount);
            }
        }
    }

    /**
     * Handle selection change when the user select a correct amount
     * @param newCalculatorValue
     */
    onSelectionChange(newCalculatorValue: CalculatorComponentValue): void {
        if (newCalculatorValue) {
            this._updateAmount(newCalculatorValue.value);
            this.calculatorResult = {equal: {...newCalculatorValue}};
        }
    }

    /**
     * Search for the possible combinations when the form is submitted
     */
    async validate(): Promise<void> {
        try {
            this.isLoading = true;
            const result: CalculatorApiResult = await this._calculatorService
                .searchCombination(5, this.amount);
            this._handleCalculatorResultFromAPI(result);
            this.isLoading = false;
        } catch (e) {
            this.isLoading = false;
            console.error(e);
            throw e;
        }
    }

    /**
     * Update amount and get combinations if the user click on the plus/minus button
     * @param action Can be plus or minus
     */
    manageAmount(action: string): void {
        if (!this.amount &&
            action === 'minus') {
            return;
        } else if (!this.amount) {
            this.amount = 0;
        }
        action === 'plus' ? this.amount++ : this.amount--;
        this.validate();
    }
}
