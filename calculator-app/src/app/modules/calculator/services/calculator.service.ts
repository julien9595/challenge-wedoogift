import {Injectable} from '@angular/core';
import {ShopApiService} from '../../../core/http/api-services/shop/shop.api.service';
import {CalculatorApiResult} from '../models/calculator-api-result.model';

@Injectable({
    providedIn: 'root'
})
export class CalculatorService {
    constructor(private _shopApiService: ShopApiService) {
    }

    async searchCombination(shopId: number,
                            amount: number): Promise<CalculatorApiResult> {
        try {
            const queryParams: {amount: number} = {amount};
            return await this._shopApiService
                .searchCombination$(shopId, queryParams)
                .toPromise();
        } catch (e) {
            console.error(e);
            throw e;
        }
    }
}
