import {CalculatorService} from './calculator.service';
import {TestBed, waitForAsync} from '@angular/core/testing';
import {ShopApiService} from '../../../core/http/api-services/shop/shop.api.service';
import {Observable} from 'rxjs';

class ShopApiServiceMock {
    searchCombination$() {}
}

describe('CalculatorService', () => {
    let service: CalculatorService;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            providers: [
                CalculatorService,
                {
                    provide: ShopApiService,
                    useClass: ShopApiServiceMock
                },
            ]
        });
        service = TestBed.inject(CalculatorService);
    }));

    it('should create', () => {
        expect(service).toBeTruthy();
    });

    describe('searchCombination()', () => {
        it('should call this._shopApiService.searchCombination$() with correct params', async () => {
           spyOn(service['_shopApiService'], 'searchCombination$')
               .and.returnValue(new Observable((observer => {
               observer.next({});
               observer.complete();
           })));
           const shopId = 1;
           const amount = 50;
           await service.searchCombination(shopId, amount);
           const expectedQueryParams = {amount: 50};
           expect(service['_shopApiService'].searchCombination$).toHaveBeenCalledWith(shopId, expectedQueryParams);
        });
    });
});
