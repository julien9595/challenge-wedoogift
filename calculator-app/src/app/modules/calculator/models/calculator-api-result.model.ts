import {CalculatorComponentValue} from './calculator-component-value.model';

export interface CalculatorApiResult {
    equal?: CalculatorComponentValue;
    ceil?: CalculatorComponentValue;
    floor?: CalculatorComponentValue;
}
